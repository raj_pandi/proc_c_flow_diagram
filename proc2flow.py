# MIT License

# Copyright (c) 2018 Rajkumar Pandi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from graphviz import Graph
import matplotlib.pyplot as plt
import re
import os

os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'

def find_func_decl(filename,k):
        func_decl = {}
        for i,line in enumerate(filename,1):
         if re.findall(r'(?<=(?<=int\s)|(?<=void\s)|(?<=string\s)|(?<=double\s)|(?<=float\s‌​)|(?<=char\s)).*?(?=\s?\()',line):
          for j in k:
           if j in line:
            if re.findall(r'^(.*[^;]{1})(\n.*)',line):
               func_decl[j] = i
        return func_decl

def find_func_sql(filename):
        filename2 = open(filename).read()
        m = re.findall(r'(EXEC)(.+?)((?:\n.+?)+?)(POSTORA)',filename2)

        f ={}
        for i in range(1,len(m)):
                x1 = m[i]
                s = x1[2].split()
                f[s[0]] = ' '.join(str(y) for y in s[1:] if not y.isdigit())
        f_swap = dict((v,k) for k,v in f.items())
        return f_swap

def find_func_call(filename2):
        func_call = {}
        for l,lines in enumerate(filename2,1):
         if re.findall(r'[\w.]+\(\)',lines):
          lines=lines.strip(' \t\n\r;()')
          func_call[lines] = l
        return func_call

def output_plot(func_sorted_decl,func_sorted_common):
        edges_dict = {}
        
        for k in range(len(func_sorted_decl)):
                list_app = []
                for i in range(len(func_sorted_common)):
                        if func_sorted_decl[k][1] < int(func_sorted_common[i][1]) < func_sorted_decl[k+1][1]:
                          list_app.append(func_sorted_common[i][0])
                edges_dict[func_sorted_decl[k][0]] = list_app
        return edges_dict
         
def main():
        
        try:
            input_file = str(input("Input the full path and name of your C file: "))
            input_file_page = str(input("Input the full path and name of your C file including page number: "))
            options = input("Enter 1) Flow Graph for functions 2) Function name with SQL query: ")
            str_read = open(input_file).read()
            k = re.findall(r'(?<=(?<=int\s)|(?<=void\s)|(?<=string\s)|(?<=double\s)|(?<=float\s‌​)|(?<=char\s)).*?(?=\s?\()',str_read)
                
            filename = open(input_file,"r")
            func_decl = find_func_decl(filename,k)
            filename.seek(0)
            func_sorted_decl = sorted(func_decl.items(), key=lambda kv: kv[1])
               
            if options == str(1):
                    func_call = find_func_call(filename)
                    func_sorted_call = sorted(func_call.items(), key=lambda kv: kv[1])
                    edges_dict = output_plot(func_sorted_decl, func_sorted_call)
            if options == str(2):
                    func_sql = find_func_sql(input_file_page)
                    func_sorted_sql = sorted(func_sql.items(),key=lambda kv: kv[1])
                    edges_dict = output_plot(func_sorted_decl, func_sorted_sql)
                        
            g = Graph('G',filename='descriptionfile.gv')
            for k,v in edges_dict.items():
                  for vv in v:
                     g.edge(k,vv)

            g.render('descriptionfile.gv',view=True)
        
        except:
                print("Dot Graph is too huge -> Use: http://www.webgraphviz.com/")
                
        
if __name__ == '__main__':
   main()                
        
