# proc*c_code2flow

This program parses any .C or .proc C(pc) program, and helps you visualize the control flow graph in Dot. All functional paths traversed through out the program is covered. 

# Packages Required
- Graphviz (Update path of environment in line no. 11 "os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'"")
- matplotlib
- re
- os

# Input files
- A .c or .pc file
- The same file with page number added (This file is generated by running add_line_num.py on the input c/pc file)

# Output files
- descriptionfile.gv(if Option 1)<br />
  graph G {<br />
    main -- "login();                         /* allow user to log into the database *"/<br />
    main -- "getsession();                       /* obtain the current session */"<br />
 }
- descriptionfile.gv(if Option 2)<br />
  graph G { <br />
	main -- "select * from student_org"
}<br />
Note: These ouput files are rendered to a graph by graphviz library

# Run
>python proc2flowbeta.py<br />

>Input the full path of your C file: hash.c<br />
>Input the full path of your C file including page number: added_pagetosrc<br />
>Enter 1) Flow Graph for functions 2) Function name with SQL query: 1<br />

The output description.gv and description.gv.pdf will be present in current working directory where proc2flow.py runs


