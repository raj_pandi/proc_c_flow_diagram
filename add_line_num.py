# -*- coding: utf-8 -*-
"""
Created on Thu Nov  8 08:28:50 2018

@author: rpandi
"""
import re

input_file = str(input("Input the full path and name of your C file: "))
out = open('added_pagetosrc','w+')
with open(input_file) as fp:
    for i, line in enumerate(fp):
        out.write('%04d %s'%(i, line))
out.close()
